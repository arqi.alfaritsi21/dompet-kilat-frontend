<h1 align='center'>Vue JS - Dompet Kilat Frontend</h1>
<p align="center">
    ·
    <a href="https://gitlab.com/arqi.alfaritsi21/dompet-kilat-frontend/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/arqi.alfaritsi21/dompet-kilat-frontend/-/issues">Request Feature</a>
  </p>


## Built With

[![Vue](https://img.shields.io/badge/Vue-v2.6.11-green)](https://github.com/vuejs/vue)
[![Bootstrap](https://img.shields.io/badge/Bootstrap-v4.5.x-blue)](https://github.com/bootstrap-vue/bootstrap-vue)

## Requirements

1. <a href="https://nodejs.org/en/download/">Node Js</a>
2. Node_modules `npm install` or `yarn install`

## Getting Started

1. Download this Project or you can type `git clone https://github.com/alfaritsi21/foodiepie-frontend.git`
2. Open app's directory in CMD or Terminal
3. Type `npm install` or `yarn install`
4. Type `npm run serve`

## Acknowledgements
- [vue-Bootstrap](https://bootstrap-vue.org/)

## License

© [Arqi Alfaritsi](https://gitlab.com/arqi.alfaritsi21/)
